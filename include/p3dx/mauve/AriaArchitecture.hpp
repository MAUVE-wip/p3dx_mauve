#ifndef MAUVE_P3DX_ARIA_ARCHITECTURE_HPP
#define MAUVE_P3DX_ARIA_ARCHITECTURE_HPP

#include <mauve/runtime.hpp>
#include <mauve/ros.hpp>

#include <aria/mauve/AriaResource.hpp>
#include <aria/mauve/AriaComponent.hpp>
#include <mauve/base/Relay.hpp>

#include <std_msgs/Float64.h>
#include <geometry_msgs/Pose2D.h>
#include <geometry_msgs/Twist.h>

namespace p3dx {
  namespace mauve {

struct AriaResourceArchitecture : public ::mauve::runtime::Architecture {

  aria::mauve::AriaResource & driver = mk_resource<aria::mauve::AriaResource>("driver");

  ::mauve::base::RelayComponent<::mauve::types::geometry::Pose2D> & relay_pose =
    mk_component<::mauve::base::RelayComponent<::mauve::types::geometry::Pose2D>>("relay_pose");

  ::mauve::base::RelayComponent<::mauve::types::geometry::UnicycleVelocity> & relay_vel =
    mk_component<::mauve::base::RelayComponent<::mauve::types::geometry::UnicycleVelocity>>("relay_vel");

  ::mauve::base::RelayComponent<double> & relay_bat =
    mk_component<::mauve::base::RelayComponent<double>>("relay_bat");

  ::mauve::base::RelayComponent<double> & relay_time =
    mk_component<::mauve::base::RelayComponent<double>>("relay_time");

  using pose_pub_t = ::mauve::ros::PublisherResource<
    ::mauve::types::geometry::Pose2D, geometry_msgs::Pose2D>;
  pose_pub_t & pose_pub = mk_resource<pose_pub_t>("pose_pub");

  using vel_pub_t = ::mauve::ros::PublisherResource<
    ::mauve::types::geometry::UnicycleVelocity, geometry_msgs::Twist>;
  vel_pub_t & vel_pub = mk_resource<vel_pub_t>("velocity_pub");

  using bat_pub_t = ::mauve::ros::PublisherResource<double, std_msgs::Float64>;
  bat_pub_t & bat_pub = mk_resource<bat_pub_t>("battery_pub");

  using time_pub_t = ::mauve::ros::PublisherResource<double, std_msgs::Float64>;
  time_pub_t & time_pub = mk_resource<time_pub_t>("time_pub");

  virtual bool driver_to_ros() {
    relay_pose.shell().read_port.connect(driver.interface().read_pose);
    relay_pose.shell().write_port.connect(pose_pub.interface().write);
    pose_pub.shell().topic = "/robot/pose";
    pose_pub.shell().conversion.set_value(
      ::mauve::ros::conversions::convert<::mauve::types::geometry::Pose2D, geometry_msgs::Pose2D>);
    relay_pose.fsm().period = ::mauve::runtime::ms_to_ns(100);

    relay_vel.shell().read_port.connect(driver.interface().read_velocity);
    relay_vel.shell().write_port.connect(vel_pub.interface().write);
    vel_pub.shell().topic = "/robot/velocity";
    vel_pub.shell().conversion.set_value(
      ::mauve::ros::conversions::convert<::mauve::types::geometry::UnicycleVelocity, geometry_msgs::Twist>);
    relay_vel.fsm().period = ::mauve::runtime::ms_to_ns(100);

    relay_bat.shell().read_port.connect(driver.interface().read_battery);
    relay_bat.shell().write_port.connect(bat_pub.interface().write);
    bat_pub.shell().topic = "/robot/battery";
    bat_pub.shell().conversion.set_value(
      ::mauve::ros::conversions::convert<double, std_msgs::Float64>);
    relay_bat.fsm().period = ::mauve::runtime::ms_to_ns(100);

    relay_time.shell().read_port.connect(driver.interface().read_time);
    relay_time.shell().write_port.connect(time_pub.interface().write);
    time_pub.shell().topic = "/robot/time";
    time_pub.shell().conversion.set_value(
      ::mauve::ros::conversions::convert<double, std_msgs::Float64>);
    relay_time.fsm().period = ::mauve::runtime::ms_to_ns(100);

    return true;
  }

  virtual bool configure_hook() override {
    driver_to_ros();
    return ::mauve::runtime::Architecture::configure_hook();
  }
};

struct AriaComponentArchitecture : public ::mauve::runtime::Architecture {

  aria::mauve::AriaComponent & driver = mk_component<aria::mauve::AriaComponent>("driver");

  using pose_pub_t = ::mauve::ros::PublisherResource<
    ::mauve::types::geometry::Pose2D, geometry_msgs::Pose2D>;
  pose_pub_t & pose_pub = mk_resource<pose_pub_t>("pose_pub");

  using vel_pub_t = ::mauve::ros::PublisherResource<
    ::mauve::types::geometry::UnicycleVelocity, geometry_msgs::Twist>;
  vel_pub_t & vel_pub = mk_resource<vel_pub_t>("velocity_pub");

  using bat_pub_t = ::mauve::ros::PublisherResource<double, std_msgs::Float64>;
  bat_pub_t & bat_pub = mk_resource<bat_pub_t>("battery_pub");

  using time_pub_t = ::mauve::ros::PublisherResource<double, std_msgs::Float64>;
  time_pub_t & time_pub = mk_resource<time_pub_t>("time_pub");

  virtual bool driver_to_ros() {
    driver.shell().pose.connect(pose_pub.interface().write);
    pose_pub.shell().topic = "/robot/pose";
    pose_pub.shell().conversion.set_value(
      ::mauve::ros::conversions::convert<::mauve::types::geometry::Pose2D, geometry_msgs::Pose2D>);

    driver.shell().velocity.connect(vel_pub.interface().write);
    vel_pub.shell().topic = "/robot/velocity";
    vel_pub.shell().conversion.set_value(
      ::mauve::ros::conversions::convert<::mauve::types::geometry::UnicycleVelocity, geometry_msgs::Twist>);

    driver.shell().voltage.connect(bat_pub.interface().write);
    bat_pub.shell().topic = "/robot/battery";
    bat_pub.shell().conversion.set_value(
      ::mauve::ros::conversions::convert<double, std_msgs::Float64>);

    driver.shell().time.connect(time_pub.interface().write);
    time_pub.shell().topic = "/robot/time";
    time_pub.shell().conversion.set_value(
      ::mauve::ros::conversions::convert<double, std_msgs::Float64>);

    driver.fsm().period = ::mauve::runtime::ms_to_ns(100);

    return true;
  }

  virtual bool configure_hook() override {
    driver_to_ros();
    return ::mauve::runtime::Architecture::configure_hook();
  }
};

  }
}

#endif
