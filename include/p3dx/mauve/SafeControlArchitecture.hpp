#ifndef MAUVE_P3DX_SAFECONTROL_ARCHITECTURE_HPP
#define MAUVE_P3DX_SAFECONTROL_ARCHITECTURE_HPP

#include <mauve/runtime.hpp>
#include <mauve/ros.hpp>
#include <unicycle_control/mauve/SafetyPilot.hpp>
#include <unicycle_control/mauve/PotentialField.hpp>
#include <hokuyo/mauve/Hokuyo.hpp>

#include "TeleopArchitecture.hpp"

#include <sensor_msgs/LaserScan.h>
#include <visualization_msgs/Marker.h>

namespace p3dx {
  namespace mauve {

bool convert_obstacle(const ::mauve::types::geometry::Pose2D& p, visualization_msgs::Marker& m) {
  m.header.frame_id = "base_link";
  m.ns = "philibot";
  m.id = 0;
  m.type = visualization_msgs::Marker::SPHERE;
  m.action = visualization_msgs::Marker::ADD;
  m.pose.position.x = p.location.x;
  m.pose.position.y = p.location.y;
  m.pose.position.z = 0;
  m.pose.orientation.x = 0.0;
  m.pose.orientation.y = 0.0;
  m.pose.orientation.z = 0.0;
  m.pose.orientation.w = 1.0;
  m.scale.x = p.theta;
  m.scale.y = p.theta;
  m.scale.z = 0.1;
  m.color.a = 1.0; // Don't forget to set the alpha!
  m.color.r = 0.0;
  m.color.g = 1.0;
  m.color.b = 0.0;
  return true;
};

struct SafeControlArchitecture : public TeleopArchitecture {

  hokuyo::mauve::Hokuyo & hokuyo = mk_component<hokuyo::mauve::Hokuyo>("hokuyo");
  unicycle_control::mauve::SafetyPilot & safety =
    mk_component<unicycle_control::mauve::SafetyPilot>("safety");
  unicycle_control::mauve::PotentialField & control =
    mk_component<unicycle_control::mauve::PotentialField>("control");

  ::mauve::runtime::SharedData<::mauve::types::sensor::LaserScan> & scan =
    mk_resource<::mauve::runtime::SharedData<::mauve::types::sensor::LaserScan>>("scan",
    ::mauve::types::sensor::LaserScan());
  ::mauve::runtime::SharedData<::mauve::types::geometry::UnicycleVelocity> & command =
    mk_resource<::mauve::runtime::SharedData<::mauve::types::geometry::UnicycleVelocity>>(
      "command", ::mauve::types::geometry::UnicycleVelocity());
  ::mauve::runtime::SharedData<::mauve::types::geometry::Point2D> & target =
    mk_resource<::mauve::runtime::SharedData<::mauve::types::geometry::Point2D>>(
      "target", ::mauve::types::geometry::Point2D());

  using scan_pub_t = ::mauve::ros::PublisherResource<::mauve::types::sensor::LaserScan,
    sensor_msgs::LaserScan>;
  scan_pub_t & scan_publisher = mk_resource<scan_pub_t>("scan_publisher");

  using obstacle_pub_t = ::mauve::ros::PublisherResource<::mauve::types::geometry::Pose2D,
    visualization_msgs::Marker>;
  obstacle_pub_t & obstacle_publisher = mk_resource<obstacle_pub_t>("obstacle_publisher", "obstacle");

  using target_sub_t = ::mauve::ros::SubscriberResource<geometry_msgs::Point, ::mauve::types::geometry::Point2D>;
  target_sub_t & target_subscriber =
    mk_resource<target_sub_t>("target_subscriber", "target");

  virtual bool hokuyo_to_ros() {
    scan_publisher.shell().topic = "/robot/scan";
    scan_publisher.shell().conversion.set_value(
      ::mauve::ros::conversions::convert<::mauve::types::sensor::LaserScan,
        sensor_msgs::LaserScan>);
    hokuyo.shell().scan.connect(scan_publisher.interface().write);
    return true;
  }

  virtual bool configure_hook() override {
    // From TeleopArchitecture
    driver_to_ros();
    joy_teleop();

    // Target
    target_subscriber.shell().write_port.connect(target.interface().write);
    target_subscriber.shell().conversion.set_value(
      ::mauve::ros::conversions::convert<geometry_msgs::Point, ::mauve::types::geometry::Point2D>);
    control.shell().goal.connect(target.interface().read);

    // Scan
    hokuyo_to_ros();
    hokuyo.shell().scan.connect(scan.interface().write);
    safety.shell().scan.connect(scan.interface().read_value);
    control.shell().scan.connect(scan.interface().read_value);

    // Command
    safety.shell().output.connect(cmd.interface().write);
    control.shell().command.connect(command.interface().write);
    safety.shell().input.connect(command.interface().read);

    // Teleop
    teleop.shell().command.connect(cmd.interface().write);
    //teleop.shell().stop.connect(driver.interface().stop);

    // Robot command
    driver.shell().vel_cmd.connect(cmd.interface().read);

    // Obstacle debug
    control.shell().obstacle.connect(obstacle_publisher.interface().write);
    obstacle_publisher.shell().conversion.set_value(convert_obstacle);

    // Periods
    hokuyo.fsm().period = ::mauve::runtime::ms_to_ns(100);
    control.fsm().sync_reach.set_clock(::mauve::runtime::ms_to_ns(100));
    safety.fsm().period = ::mauve::runtime::ms_to_ns(10);

    // Properties
    control.shell().obstacle_distance = 1.5;
    control.shell().safety_distance = 1.0;

    return ::mauve::runtime::Architecture::configure_hook();
  }
};

  }
}

#endif
