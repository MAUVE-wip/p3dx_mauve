#ifndef MAUVE_P3DX_TELEOP_ARCHITECTURE_HPP
#define MAUVE_P3DX_TELEOP_ARCHITECTURE_HPP

#include <mauve/runtime.hpp>
#include <mauve/ros.hpp>

#include "AriaArchitecture.hpp"

#include <unicycle_control/mauve/Teleop.hpp>
#include <sensor_msgs/Joy.h>

namespace p3dx {
  namespace mauve {

struct TeleopArchitecture : public AriaComponentArchitecture {

  unicycle_control::mauve::Teleop & teleop = mk_component<unicycle_control::mauve::Teleop>("teleop");

  ::mauve::ros::SubscriberResource<sensor_msgs::Joy, ::mauve::types::sensor::Joy> &
    joy_subscriber = mk_resource<
      ::mauve::ros::SubscriberResource<sensor_msgs::Joy,::mauve::types::sensor::Joy> >(
        "joy_subscriber");

  using cmd_t = ::mauve::runtime::SharedData<::mauve::types::geometry::UnicycleVelocity>;
  cmd_t & cmd = mk_resource<cmd_t>("command", ::mauve::types::geometry::UnicycleVelocity { 0.0, 0.0 });

  virtual bool joy_teleop() {
    // Properties for Logitech Gamepad F710
    teleop.shell().linear_axis = 1;
    teleop.shell().angular_axis = 0;
    teleop.shell().stop_button = 1;
    teleop.shell().inverse_stop = true;
    teleop.shell().fast_button = 0;
    // ROS topic
    teleop.shell().joystick.connect(joy_subscriber.interface().read);
    // ROS Properties
    joy_subscriber.shell().topic = "joy";
    joy_subscriber.shell().conversion.set_value(
      ::mauve::ros::conversions::convert<sensor_msgs::Joy, ::mauve::types::sensor::Joy>);
    // Teleop period
    teleop.fsm().period = ::mauve::runtime::ms_to_ns(10);

    return true;
  }

  virtual bool configure_hook() override {
    // Connect command to driver
    teleop.shell().command.connect(cmd.interface().write);
    driver.shell().vel_cmd.connect(cmd.interface().read);

    joy_teleop();
    driver_to_ros();
    return ::mauve::runtime::Architecture::configure_hook();
  }
};

  }
}

#endif
