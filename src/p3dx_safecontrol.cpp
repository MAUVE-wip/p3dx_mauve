#include <mauve/runtime.hpp>
#include "p3dx/mauve/SafeControlArchitecture.hpp"

int main() {
  std::stringstream config;
  config << "default:" << std::endl;
  config << "  type: stdout" << std::endl;
  config << "  level: info" << std::endl;
  mauve::runtime::AbstractLogger::initialize(config);

  auto * archi = new p3dx::mauve::SafeControlArchitecture();
  auto depl = mk_abstract_deployer(archi);

  archi->configure();
  depl->create_tasks();
  depl->activate();
  depl->start();

  depl->loop();

  depl->stop();
  archi->cleanup();

  return 0;
}
